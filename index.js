const axios = require('axios');
const cheerio = require('cheerio');

const url1 = "https://www.amazon.es/s?k=cama+de+perro&page=1&__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&qid=1606143283&ref=sr_pg_1";
const url2 = "https://www.amazon.es/s?k=cama+de+perro&page=2&__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&qid=1606143283&ref=sr_pg_2";


const camas_de_perros = [];

axios(url1)
    .then(response => {
        const html = response.data;

        const $ = cheerio.load(html);
        const camas = $('div.s-result-item');
        camas.each(function () {
            const desc = $(this).find('h2 > a > span').text();
            const precio = $(this).find("span.a-offscreen").text().split("€")[0];

            const cama = {
                descripcion: desc,
                precio: precio.replace(',', '.') * 1
            };

            camas_de_perros.push(cama);

        })


    })
    .then(() => axios(url2))
    .then(response => {
        const html = response.data;

        const $ = cheerio.load(html);
        const camas = $('div.s-result-item');
        camas.each(function () {
            const desc = $(this).find('h2 > a > span').text();
            const precio = $(this).find("span.a-offscreen").text().split("€")[0];

            const cama = {
                descripcion: desc,
                precio: precio.replace(',', '.') * 1
            };

            camas_de_perros.push(cama);

        })

    })
    .then(() => {
        console.log(JSON.stringify(camas_de_perros));

    })
    .catch(err => console.log(err));
